import math
import sys

def areaOfCircle(length, radius):
    x = math.sqrt(2) * radius # distance from corner of the square to given circle
    diagOfReqCir = math.sqrt(2) * length - (2 * x) - (2 * radius) #diameter of required circle
    area = math.pi * diagOfReqCir / 2 * diagOfReqCir / 2
    return "{:.1f}".format(area)

def areaOfSquare(length, radius):
    x = math.sqrt(2) * radius # distance from corner of the square to given circle
    diagOfReqSqr = math.sqrt(2) * length - (2 * x) - (2 * radius) #diagonal of required square
    area = diagOfReqSqr / math.sqrt(2) * diagOfReqSqr / math.sqrt(2)
    return "{:.1f}".format(area)

def inscribed_figures(length, radius, shape):
    if shape == "CIR" and 2 * radius < length:
        return areaOfCircle(length, radius)
    elif shape == "SQR" and 2 * radius < length:
        return  areaOfSquare(length, radius)
    else:
        return "please enter correct values"

print(inscribed_figures(int(sys.argv[1]), int(sys.argv[2]), sys.argv[3]))
